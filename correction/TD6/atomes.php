<?php
return [
	[
		'nom' => 'hydrogene',
		'label' => 'H',
		'cout' => 6
	],
	[
		'nom' => 'oxygene',
		'label' => 'O',
		'cout' => 2
	],
	[
		'nom' => 'sodium',
		'label' => 'Na',
		'cout' => 4
	],
	[
		'nom' => 'chlore',
		'label' => 'Cl',
		'cout' => 7
	]
];