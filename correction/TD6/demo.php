<?php
$molecules = [
	'h2o' =>[
		'cout' => 20,
		'Atome' =>[
			'h' => 2,
			'o' => 1
		]
	],
	'NaCl' =>[
		'cout' => 10,
		'Atome' =>[
			'Na' => 1,
			'Cl' => 1
		]
	],
	'h2o2' =>[
		'cout' => 30,
		'Atome' =>[
			'h' => 2,
			'o' => 2
		]
	],
];
// Trie par cout de production
// uasort($molecules, function($a, $b){
// 	if($a['cout'] == $b['cout']){
// 		return 0;
// 	}
// 	return ($a['cout'] < $b['cout']) ? -1 : 1;
// });
// var_dump($molecules);

// trie par nb d'atomes
uasort($molecules, function($a, $b){
	if(array_sum($a['Atome']) == array_sum($b['Atome'])){
		return 0;
	}
	return (array_sum($a['Atome']) < array_sum($b['Atome'])) ? -1 : 1;
});
var_dump($molecules);

