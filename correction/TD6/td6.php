<?php

$hydrogene = [
	'nom' => 'hydrogene',
	'label' => 'H',
	'cout' => 6
];
$oxygene = [
	'nom' => 'oxygene',
	'label' => 'O',
	'cout' => 2
];
$sodium = [
	'nom' => 'sodium',
	'label' => 'Na',
	'cout' => 4
];
$chlore = [
	'nom' => 'chlore',
	'label' => 'Cl',
	'cout' => 7
];

$molecules = [
	[
		'nom' => 'eau',
		'formule' => 'H2O',
		'Atome' =>[
			&$hydrogene,
			&$hydrogene,
			&$oxygene
		],
	],
	[
		'nom' => 'chlorure de sodium',
		'formule' => 'NaCl',
		'Atome' =>[
			&$sodium,
			&$chlore
		],
	],
	[
		'nom' => 'Dioxygene',
		'formule' => 'o2',
		'Atome' =>[
			&$oxygene,
			&$oxygene
		],
	],
	[
		'nom' => 'Peroxyde d\'hydrogène',
		'formule' => 'H2O2',
		'Atome' =>[
			&$hydrogene,
			&$hydrogene,
			&$oxygene,
			&$oxygene,
		],
	],
];
// trie par nb d'atomes
// uasort($molecules, function($a, $b){
// 	if (count($a['atomes']) == count($b['atomes'])){
// 		return 0;
// 	}
// 	return (count($a['atomes']) < count($b['atomes'])) ? -1 : 1;
// });
// var_dump($molecules);

// trie par cout de production 
uasort($molecules, function($a, $b){
	$coutA = 0;
	$coutB = 0;
	foreach ($a['Atome'] as $key => $atome) {
		$coutA += $atome['cout'];
	}
	foreach ($b['Atome'] as $key => $atome) {
		$coutB += $atome['cout'];
	}
	return ($coutA == $coutB) ? 0 : (($coutA < $coutB) ? -1 : 1);
});

