<?php

//********** STUPID **********//

//LE SINGLETON
final class Singleton
{
    private static $instance;

    public static function getInstance(){
        if(null == static::$instance){
            static::$instance = new Static();
        }
        return static::$instance;
    }
    private function __construct(){}
    private function __clone(){}
    private function __wakeup(){}
}

Singleton::getInstance();

//LE TIGHT COUPLING
class Maison{
    public function __construct()
    {
        $this->porte = new Porte;
        $this->fenetre = new Fenetre;
    }
}

//INJECTION DE DEPENDANCE
class Maison{
    public function __construct(Porte $porte, Window $window){
        $this->porte = $porte;
        $this->window = $window;
    }
}

//********** SOLID **********//

//LISKOV

class A{
    public function foo($a, $b){}
}

class B extends A{
    /* ERREUR STRICT */
    public function foo($a,$b, $c){}
}