<?php
namespace Swf3;

class Container{
	private $providers = array();
	private $params = array();

	public function set($serviceName, $value){
		if(!$this->has($serviceName)){
			$this->providers[$serviceName] = $value;
		}
	}
	public function has($serviceName){
		return isset($this->providers[$serviceName]);
	}
	public function get($serviceName){
		if($this->has($serviceName)){
			$params = '';
			
			$service = (new $this->providers[$serviceName])->create();
			if ($this->hasParams($serviceName)){
				foreach($this->params[$serviceName] as $key => $params){
					$service->$key = $params;
				}
			}
			return $service;
		}
	}

	public function setParams($serviceName, $paramName, $value){
		$this->params[$serviceName][$paramName] = $value;
	}
	public function getParam($serviceName, $paramName){
		return $this->params[$serviceName][$paramName];
	}
	public function hasParams($serviceName){
		return isset($this->params[$serviceName]);
	}
}