<?php
namespace Swf3;

abstract class FactoryMethod{
	abstract protected function createLogger();
	public function create(){
		return $this->createLogger();
	}
}