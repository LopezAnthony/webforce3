<?php
namespace Swf3;

class Logger{
	const INFO = 'info';
	const DEBUG = 'debug';
	const WARNING = 'warning';

	private $logStorage;

	public function __construct(){
		//ici je pourrais définir un storage par défault
	}
	public function __set($key, $value){ // pas bien
		$qualifiedClass = 'Swf3\\'.ucfirst($value).'Storage';
		$this->$key = new $qualifiedClass;
	}
	public function debug($message){
		$this->log(Logger::DEBUG, $message);
	}
	public function info($message){
		$this->log(Logger::INFO, $message);
	}
	public function warning($message){
		$this->log(Logger::WARNING, $message);
	}

	protected function log($level, $message){
		if(!$this->logStorage){
			throw new \Exception('Storage is not set');
		}
		$this->logStorage->store($level, $message);
	}
	public function setLogStorage(StorageInterface $logStorage){
		$this->logStorage = $logStorage;
	}
}