<?php
namespace Swf3;
interface LoggerInterface{
	public function info($message);
	public function debug($message);
}