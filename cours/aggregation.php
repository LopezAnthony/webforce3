<?php

class Log{
    protected $file;

    public function setFile(File $file){
        $this->file = $file;
    }

    public function getFile(){
        return $this->file;
    }
}

$file = new File;
$log = new Log;
$log->setFile($file);
$content = $log->getFile()->read();

