<?php
require 'Log.php';


//ASSIOCIATION PERMANENTE
class Data{
    public function __construct(Log $log){}
    public function bar(Log $log){}
    public function foo(Log $log){}
}

//ASSOCIATION PONCTUEL
class Data{
    public function foo(Log $log){
        $log->methode();
    }
    public function bar(){
        return "blabla";
    }
}

$data = new Data;
$data->foo(new Log);
$data->bar();