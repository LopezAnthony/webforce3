<?php

class Animal
{
    public $public; //J'ai accès à cette attribut partout,
    // même en dehors de la classe ($animal->public ok)

    protected $attribut = "Je suis"; //donner une valeur ici, c'est mal
    //Si j'hérite de la classe Animal, j'aurais accès à cette attribut dans la classe fille

    private $private; //j'ai accès à cette attribut uniquement dans cette classe

    public function methode(){
        return $this->attribut;
    }

    protected function marcher(){
        return ' je marche ';
    }
}

class Chien extends Animal
{
    public function methode(){
        $str = parent::methode();
        $str .= ' un ' . __CLASS__ . '<br>';
        return $str;
    }
}

$chien = new Chien;
echo $chien->marcher();