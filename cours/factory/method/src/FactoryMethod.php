<?php
namespace Swf3;

abstract class FactoryMethod{
	abstract protected function createLogger($strategy);
	public function create($strategy){
		return $this->createLogger($strategy);
	}
}