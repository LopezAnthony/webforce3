<?php
namespace Swf3;

class LoggerFactory extends FactoryMethod{
	public function createLogger($strategy){
		$qualifiedClass = 'Swf3\\'.ucfirst($strategy).'Storage';
		if(class_exists($qualifiedClass)){
			$logger = new Logger;
			$logger->setLogStorage(new $qualifiedClass);
			return $logger;
		}else{
			throw new \Exception("Bad strategy", 1);
		}
	}
}