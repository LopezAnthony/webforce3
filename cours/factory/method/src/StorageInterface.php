<?php
namespace Swf3;

interface StorageInterface{
	public function store($level, $message);
}