<?php
namespace Swf3;
class SqliteLoggerFactory{
	public function createLogger(){
		$logger = new Logger();
		$logger->setLogStorage(new SqliteStorage);
		return $logger;
	}
}