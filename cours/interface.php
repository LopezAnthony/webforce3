<?php

interface Entretenable
{
    public function nettoyer();
    public function repeindre();
}
interface Reparable{
    public function reparer();
}

class Vehicule implements Entretenable, Reparable {
    private $propre;
    public function nettoyer(){}
    public function reparer(){}
    public function repeindre(){}
}

class Maison implements Entretenable, Reparable {}

$voiture = new Vehicule;