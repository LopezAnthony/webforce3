<?php
use \Swf3\SqliteLogger;
use \Swf3\Controller;

class App{
	private $controller;

	public function __construct(){
		$this->controller = new Controller(new SqliteLogger);
		$this->controller->run();
	}
}