<?php
namespace Swf3;
use \Psr\Log\LoggerInterface;

class Controller{
	private $logger;

	public function __construct(LoggerInterface $logger = null){
		$this->logger = $logger;
	}

	public function run(){
		if($this->logger){
			$this->logger->info('Nouvel utilisateur sur le site');
		}
	}
}