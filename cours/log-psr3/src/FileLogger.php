<?php
namespace Swf3;
use Psr\Log\AbstractLogger;

class FileLogger extends AbstractLogger{
	private $filename;
	public function __construct(){
		$this->setFilename('log.txt');
	}
	private function setFilename($filename){
		$this->filename = $filename;
	}
	public function log($level, $message, array $context = array()){
		$this->write($this->formatLog($level, $message));
	}
	public function formatLog($level, $message){
		return "[{$this->getLogTime()}] [{$level}] {$message}" . PHP_EOL;
	}
	protected function write($message){
		file_put_contents($this->filename, $message, FILE_APPEND);
	}
	protected function getLogTime(){
		$date = new \DateTime('NOW');
		return $date->format('d/m/Y H:i:s');
	}
}