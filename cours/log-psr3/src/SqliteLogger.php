<?php
namespace Swf3;

use Psr\Log\AbstractLogger;

class SqliteLogger extends AbstractLogger{
	private $bdd;

    public function __construct(){
        $this->setBdd();
        $this->createTable();
    }
    public function log($level, $message, array $context = array()){
        $this->insert($level, $message);
    }
    private function setBdd(){
        try{
            $pdo = new \PDO('sqlite:database.sqlite');
            $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
            $this->bdd = $pdo;
        } catch(Exception $e) {
            echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
            die();
        }
    } 
    private function createTable(){
        $this->bdd->query("CREATE TABLE IF NOT EXISTS log (
                                id            INTEGER         PRIMARY KEY AUTOINCREMENT,
                                message       VARCHAR( 250 ),
                                level         VARCHAR( 250 ),
                                created       DATETIME
                            );");
    }
    
    private function insert($level, $message){
        $stmt = $this->bdd->prepare("INSERT INTO log (message, level, created) VALUES (:message, :level, :created)");
        $result = $stmt->execute(array(
            'message'       => $message,
            'level'         => $level,
            'created'       => $this->getLogTime()
        ));
        $this->getLogs();
    }
    private function getLogs(){
        $stmt = $this->bdd->prepare("SELECT * FROM log");
        $stmt->execute();
        $result = $stmt->fetchAll();
        var_dump($result);
    }
    protected function getLogTime(){
		$date = new \DateTime('NOW');
		return $date->format('d/m/Y H:i:s');
	}
}