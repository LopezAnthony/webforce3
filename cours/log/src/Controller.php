<?php
namespace Swf3;

class Controller{
	private $logger;

	public function __construct($logger = null){
		$this->logger = $logger;
	}

	public function run(){
		if($this->logger){
			$this->logger->info('Nouvel utilisateur sur le site');
		}
	}
}