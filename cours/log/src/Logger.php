<?php
namespace Swf3;

abstract class Logger{
	const INFO = 'info';
	const DEBUG = 'debug';
	const WARNING = 'warning';

	public function debug($message){
		$this->log(Logger::DEBUG, $message);
	}
	public function info($message){
		$this->log(Logger::INFO, $message);
	}
	public function warning($message){
		$this->log(Logger::WARNING, $message);
	}
	private function log($level, $message){
		return $this->store($level, $message);
	}

	abstract protected function store($level, $message);

	protected function getLogTime(){
		$date = new \DateTime('NOW');
		return $date->format('d/m/Y H:i:s');
	}
}