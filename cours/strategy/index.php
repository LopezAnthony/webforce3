<?php
require './vendor/autoload.php';

use Swf3\Logger;
use Swf3\FileStorage;
use Swf3\SqliteStorage;

class TestController{

	private $logger;

	public function __construct(){
		$this->logger = new Logger;
	}
	public function testLogger(){
		$this->logger->setLogStorage(new FileStorage);
		$this->logger->debug('Log stocké dans un fichier');

		$this->logger->setLogStorage(new SqliteStorage);
		$this->logger->info('Log stocké dans la bdd');
	}
}
$ctl = new TestController;
$ctl->testLogger();