<?php

$hydrogene = [
  'nom' => 'hydrogen',
  'label' => 'H',
  'cout' => 6
];

$oxygene = [
    'nom' => 'oxygene',
    'label' => 'O',
    'cout' => 2
];

$sodium = [
    'nom' => 'sodium',
    'label' => 'Na',
    'cout' => 4
];

$chlore = [
    'nom' => 'chlore',
    'label' => 'Cl',
    'cout' => 7
];

$molecule = [
    [
        'nom' => 'eau',
        'formule' => 'H2O',
        'Atome' => [
            &$hydrogene,
            &$hydrogene,
            &$oxygene,
        ],
    ],
    [
        'nom' => 'chlorure de sodium',
        'formule' => 'NaCl',
        'Atome' => [
            &$chlore,
            &$sodium,
        ],
    ],
    [
        'nom' => 'dioxygene',
        'formule' => 'O2',
        'Atome' => [
            &$oxygene,
            &$oxygene,
        ],
    ],
    [
        'nom' => 'peroxyde d\'hydrogene',
        'formule' => 'H2O2',
        'Atome' => [
            &$hydrogene,
            &$hydrogene,
            &$oxygene,
            &$oxygene,
        ],
    ],
];


//uasort($molecule, function($a, $b){
//    if(count($a['atomes']) == count($b['atomes'])){
//        return 0;
//    }
//    return (count($a['atomes']) < count($b['atomes'])) ? -1 : 1;
//});
//var_dump($molecule);

uasort($molecules, function($a, $b){
   $coutA = 0;
   $coutB = 0;
   foreach ($a['Atome'] as $key => $atome){
       $coutA += $atome['cout'];
   }
   foreach ($b['Atome'] as $key => $atome){
       $coutB += $atome['cout'];
   }
   if($coutA == $coutB){
       return 0;
   }
   return($coutA < $coutB) ? -1 : 1;
});

var_dump($molecule);