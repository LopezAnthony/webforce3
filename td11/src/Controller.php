<?php

namespace App;

class Controller
{
    public function errorController($log, $debug, $info){
        FileLogger::errorFile($log, $debug, $info);
        SqliteLogger::insertLog($log, $debug, $info);
    }
}