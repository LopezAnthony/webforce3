<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 10/10/2017
 * Time: 12:23
 */

namespace App;


abstract class Logger implements LoggerInterface
{
    public function log($log)
    {
        return $this->log = $log;
    }

    public function debug($debug)
    {
        return $this->debug = $debug;
    }

    public function info($info)
    {
        return $this->info = $info;
    }
}