<?php

namespace App;

interface LoggerInterface
{
    public function log($log);
    public function debug($debug);
    public function info($info);
}
