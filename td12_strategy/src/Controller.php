<?php

namespace App;

use App\LogContext;
use App\SqliteLogger;
use App\FileLogger;

class Controller
{

    public function __construct()
    {
        $this->logger = new LogContext(new SqliteLogger());
    }

    public function run(){
        $this->logger->setStrategy(new SqliteLogger());
        $this->logger->debug('hello world');
        $this->logger->setStrategy(new SqliteLogger());
        $this->logger->info('test');
    }
}
require '../vendor/autoload.php';

$controller = new Controller();
$controller->run();