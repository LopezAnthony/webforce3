<?php
namespace App;

class FileLogger implements LoggerInterface
{
    private $filename;
    public function __construct(){
        $this->setFilename('log.txt');
    }
    private function setFilename($filename){
        $this->filename = $filename;
    }
    public function store($level, $message){
        $this->write($this->formatLog($level, $message));
    }
    public function formatLog($level, $message){
        return "[{$level}] {$message}" . PHP_EOL;
    }

    public function log($level, $message){
        return $this->store($level, $message);
    }

    protected function write($message){
        file_put_contents($this->filename, $message, FILE_APPEND);
    }
}