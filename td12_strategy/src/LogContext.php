<?php

namespace App;


class LogContext
{
    private $strategy;
    const INFO = 'info';
    const DEBUG = 'debug';
    const WARNING = 'warning';

    public function __construct(LoggerInterface $strategy)
    {
        $this->setStrategy(new $strategy);
    }

    public function debug($message){
        $this->strategy->log(LogContext::DEBUG, $message);
    }
    public function info($message){
        $this->strategy->log(LogContext::INFO, $message);
    }
    public function warning($message){
        $this->strategy->log(LogContext::WARNING, $message);
    }


    public function setStrategy(LoggerInterface $strategy){
        $this->strategy = $strategy;
    }

}