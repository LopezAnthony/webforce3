<?php

namespace App;

interface LoggerInterface
{
    public function store($level, $message);
}