<?php

namespace Swf3;
use Swf3\FileLoggerFactory;
use Swf3\SqliteLoggerFactory;


class Container
{
    private $registry = [];

    public function set($key, $resolver){
        var_dump($resolver);
        if($this->has($key) == true){
            throw new \Exception('KEY already exists');
        }
        $this->registry[$key] = $resolver;
    }

    public function get($key){
        if($this->has($key) == false){
            throw new \Exception('KEY dosen\'t exist');
        }
        $factory = $this->registry[$key];
        return (new $factory)->createLogger();
    }

    public function has($key){
        if(isset($this->registry[$key])){
            return true;
        }
        return false;
    }

}