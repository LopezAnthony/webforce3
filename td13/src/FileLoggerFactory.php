<?php
namespace Swf3;

class FileLoggerFactory{
	public function createLogger(){
		$logger = new Logger();
		$logger->setLogStorage(new FileStorage);
		return $logger;
	}
}

