<?php
namespace Swf3;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class Logger implements LoggerInterface
{
	private $logStorage;

	public function __construct(){
		//ici je pourrais définir un storage par défault
	}
	public function debug($message, array $context = array()){
		$this->log(LogLevel::DEBUG, $message);
	}
	public function info($message, array $context = array()){
		$this->log(LogLevel::INFO, $message);
	}
	public function warning($message, array $context = array()){
		$this->log(LogLevel::WARNING, $message);
	}

    public function emergency($message, array $context = array()){
        $this->log(LogLevel::EMERGENCY, $message);
    }
    public function alert($message, array $context = array()){
        $this->log(LogLevel::ALERT, $message);
    }
    public function critical($message, array $context = array()){
        $this->log(LogLevel::CRITICAL, $message);
    }
    public function error($message, array $context = array()){
        $this->log(LogLevel::ERROR, $message);
    }
    public function notice($message, array $context = array()){
        $this->log(LogLevel::NOTICE, $message);
    }

    public function log($level, $message, array $context = array()){
		if(!$this->logStorage){
			throw new \Exception('Storage is not set');
		}
		$this->logStorage->store($level, $message);
	}
	public function setLogStorage(StorageInterface $logStorage){
		$this->logStorage = $logStorage;
	}
}