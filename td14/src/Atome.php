<?php

class Atome
{
    protected $name;
    protected $symbol;

    /**
     * Atome constructor.
     * @param $name
     * @param $symbol
     */
    public function __construct($name, $symbol)
    {
        $this->name = $name;
        $this->symbol = $symbol;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param mixed $symbol
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

}