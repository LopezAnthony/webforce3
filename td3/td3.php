
<?php
/*
4 types scalaires :
    boolean
    integer
    float (nombre à virgule flottante, i.e. double)
    string
2 types composés :
    array
    object
Et finalement, 2 types spéciaux :
    resource
    NULL
*/


//TYPAGE

    //var_dump( 1+1); => INT(2)
    //var_dump(010 + 2); => INT(10), 010 interprété en tant que octal et donc 010 donne 8
    //var_dump(012801); => PHP Parse error:  Invalid numeric literal double quote expected
    //var_dump(39 % 3); => INT(0)
    //var_dump(2**3); => int(8)
    //var_dump(1.5 - 1.0); FLOAT(0.5)
    //var_dump(0.6 == 0.7 - 0.1); => BOOL(true)
    //var_dump(.7 == .8 - .1); => BOOL(FALSE) précision décimal IEEE 754 voir BC Math et GMP pour comparaison précise
    //var_dump('text'{2}); => string(1)"x"

    //$array = [1, 3 => 2, 3];
    //$array[0] = 4;
    //$array[] = 5;

    //var_dump($array);
    //=>  [0] => 4 INT
    //    [3] => 2 INT indice d'array redéfinie
    //    [4] => 3 INT indice 4 incrémentation
    //    [5] => 5 INT equivalent d'un push


//TRANSTYPAGE

    //var_dump(1 + .7); => float(1.7)
    //var_dump(12345678901234567890); => float(1.2345678901235E+19) dépassement d'entier sur un système 64-bit valeur maximal 9223372036854775807
    //var_dump(10 + null); => INT(10)
    //var_dump("0.50 euros + 2.50 euros" == .5); => BOOL(true)
    //var_dump([] > 0); => BOOL(true)
    //var_dump([] == null); => BOOL(true)
    //var_dump([1, 2] + [3, 4]); => array(2) { [0]=> int(1) [1]=> int(2) } array_merge pour fusionner des tableaux
    //var_dump((int) 'Bonjour'); => int(0)
    //var_dump((array) 12); => array(1) { [0]=> int(12) }
