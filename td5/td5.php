<?php

/**
 * Fonction qui change le message de log d'erreur
 * Elle retournera un message custom en cas d'erreur affichant la date et une INFO personnalisée.
 * Elle sauvegardera le message dans le errorlog.txt
 * Passé en paramètre de set_error_handler()
 */
function errorlog(){
    $error = date('[Y-m-d H:m:s]') . " INFO: Please be careful about the responsability of your functions \n" . date('[Y-m-d H:m:s]') . " INFO: Try not to forget documentation \n";
    echo $error;
    error_log($error, 3,'errorlog.txt');
}
set_error_handler('errorlog');

$test=2;
if ($test>1) {
    trigger_error("error !!!!!!!!!");
}

