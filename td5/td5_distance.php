<?php

/**
 * Fonction permettant de calculer la distance entre deux point
 *
 * @param $a array(coordonnée : x,y,z)
 * @param $b array(coordonnée: x,y,z)
 * @return float résultat distance
 */
function calculeDistance($a, $b){
    return sqrt(($a[0]-$b[0])**2+($a[1]-$b[1])**2+($a[2]-$b[2])**2);
}

$arrayA = array(1, 2, 3);
$arrayB = array(3, 4, 5);
echo calculeDistance($arrayA, $arrayB);