<?php

/**
 * Fonction permettant de calculé une factiorielle.
 * @param $n INT attend le factiorielle recherché
 * @return INT le résultat
 */
function factorielle($n)
{
    /**
     * version ternère :
     *  return $n > 1 ? $n * Factoriel($n-1):1;
     */
    if($n > 1){
        return $n * factorielle($n - 1);
    }else{
        return 1;
    }
}

$fact = 3;
echo "La factiorrielle de $fact est égal à " . factorielle($fact);

