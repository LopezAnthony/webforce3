<?php

$molecules = [
    [
        'name' => 'H2O',
        'cout' => 4,
        'H' => 2,
        'O' => 1
    ],
    [
        'name' => 'NaCl',
        'cout' => 2,
        'Na' => 1,
        'Cl' => 1
    ],
    [
        'name' => 'CO2',
        'cout' => 2,
        'C' => 1,
        'O' => 2
    ],
    [
        'name' => 'NH3',
        'cout' => 4,
        'N' => 1,
        'H' => 3
    ]
];

usort($molecules,function($a,$b){
    $c = $b['cout'] - $a['cout'];
    $c .= strcmp($a['name'],$b['name']);
    return $c;
});

echo "<pre>";
print_r($molecules);
echo "</pre>";