<?php
require 'vendor/digitalnature/php-ref/ref.php';

class Atomes
{
    protected $nom;
    protected $symbole;

    /**
     * Atome constructor.
     * @param $nom
     * @param $symbole
     */
    public function __construct($nom = '', $symbole = '')
    {
        $this->setNom($nom);
        $this->setSymbole($symbole);
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param mixed $symbole
     */
    public function setSymbole($symbole)
    {
        $this->symbole = $symbole;
    }


    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getSymbole()
    {
        return $this->symbole;
    }


}

class Categorie
{
    /**
     * @var STRING
     */
    private $categorie;

    /**
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }
}

class Type
{
    private $type;

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}

class Experience
{
    public function isViable($date, $numero, $molecules){
        return true;
    }
}

class Molecule
{
    private $name;
    protected $formule;
    protected $atomes;
    protected $type;
    protected $categorie;
    private $cout;

    /**
     * Molecule constructor.
     * @param $name
     * @param $formule
     * @param $atomes
     * @param $type
     * @param $categorie
     * @param $cout
     */
    public function __construct($name = '', $formule = null,array $atomes, $type, $categorie = '')
    {
        $this->name = $name;
        if($formule){
            $this->setFormule($formule);
        }else{
            $this->genrateFormule();
        }
        $this->setAtomes($atomes);
        $this->setType($type);
    }

    /**
     * @return mixed
     */
    public function getFormule()
    {
        return $this->formule;
    }

    /**
     * @param mixed $formule
     */
    public function setFormule($formule)
    {
        $this->formule = $formule;
    }

    public function genrateFormule()
    {

    }

    /**
     * @return mixed
     */
    public function getAtomes()
    {
        return $this->atomes;
    }

    /**
     * @param mixed $atomes
     */
    public function setAtomes($atomes)
    {
        $this->atomes = $atomes;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCout()
    {
        return $this->cout;
    }

    /**
     * @param mixed $cout
     */
    public function setCout($cout)
    {
        $this->cout = $cout;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

}

class Fusion
{
    private $chercheur;

    /**
     * @return mixed
     */
    public function getChercheur()
    {
        return $this->chercheur;
    }

    /**
     * @param mixed $chercheur
     */
    public function setChercheur($chercheur)
    {
        $this->chercheur = $chercheur;
    }

    public function fusionMolecule($molecule1, $molecule2){
        return true;
    }
}

$hydrogene = new Atome;
var_dump($hydrogene);

$categorie = new Categorie;
var_dump($categorie);

$type = new Type;
var_dump($type);

$experience = new Experience;
var_dump($experience);

$molecule = new Molecule;
var_dump($molecule);

$fusion = new Fusion;
var_dump($fusion);
