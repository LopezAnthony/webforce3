<?php
namespace App;

class Atome
{
    protected $nom;
    protected $symbole;
    protected $cout;

    /**
     * Atome constructor.
     * @param $nom
     * @param $symbole
     */
    public function __construct($nom = '', $symbole = '', $cout = 1)
    {
        $this->setNom($nom);
        $this->setSymbole($symbole);
        $this->setCout($cout);
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @param mixed $symbole
     */
    public function setSymbole($symbole)
    {
        $this->symbole = $symbole;
    }


    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getSymbole()
    {
        return $this->symbole;
    }

    /**
     * @return mixed
     */
    public function getCout()
    {
        return $this->cout;
    }

    /**
     * @param mixed $cout
     */
    public function setCout($cout)
    {
        $this->cout = $cout;
    }



}