<?php
namespace App;

class Categorie
{
    /**
     * @var STRING
     */
    protected $categorie;

    public function __construct($categorie)
    {
        $this->setCategorie($categorie);
    }

    /**
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }
}