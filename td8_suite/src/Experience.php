<?php
namespace App;


class Experience
{
    protected $id;
    protected $date;
    protected $chercheur;
    protected $type;
    protected $molecule;

    /**
     * Experience constructor.
     * @param $id
     * @param $date
     * @param $chercheur
     * @param $type
     */
    public function __construct($id, $date, $chercheur, $type, $molecule, $name)
    {
        $this->setId($id);
        $this->setDate($date);
        $this->setChercheur($chercheur);
        $this->setType($type);
        $this->fusionner($molecule, $name);
    }


    public function fusionner($newMol, $name){
        $this->setMolecule(new Molecule($name, $formule = null, $newMol, 'Synthetique', 'Glucide'));
        return true;
    }

    /**
     * @return mixed
     */
    public function getMolecule()
    {
        return $this->molecule;
    }

    /**
     * @param mixed $molecules
     */
    public function setMolecule(Molecule $molecule)
    {
        $this->molecule = $molecule;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getChercheur()
    {
        return $this->chercheur;
    }

    /**
     * @param mixed $chercheur
     */
    public function setChercheur($chercheur)
    {
        $this->chercheur = $chercheur;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public function isValid(Molecule $molecule)
    {
        $cout = 0;
        foreach ($molecule->getAtomes() as $atome){
            $cout += $atome->getCout();
        }
        return ($cout % 2 == 0) ? true : false;
    }
}