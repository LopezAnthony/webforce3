<?php
namespace App;


class Fusion
{
    public function getFusion(Molecule $molecule1,Molecule $molecule2){
        $array1 = $molecule1->getAtomes();
        $array2 = $molecule2->getAtomes();
        $newMol = array_merge($array1, $array2 );
        unset($molecule1, $molecule2);
        return $newMol;
    }
}