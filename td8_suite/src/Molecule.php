<?php
namespace App;

use Zend\Stdlib\DateTime;

class Molecule
{
    protected $name;
    protected $formule;
    protected $atomes;
    protected $type;
    protected $categorie;
    protected $cout;

    /**
     * Molecule constructor.
     * @param $name
     * @param $formule
     * @param $atomes
     * @param $type
     * @param $categorie
     * @param $cout
     */
    public function __destruct()
    {
        echo " La molecule " . $this->getName() . " à été détruite à " . date(' Y-d ;');
    }

    public function __construct($name = '', $formule = null,array $atomes, $type, $categorie = '')
    {
        $this->name = $name;
        if($formule){
            $this->setFormule($formule);
        }else{
            $this->generateFormule($atomes);
        }
        $this->setAtomes($atomes);
        $this->setType($type);
        $this->setCategorie($categorie);
    }

    /**
     * @return mixed
     */
    public function getFormule()
    {
        return $this->formule;
    }

    /**
     * @param mixed $formule
     */
    public function setFormule($formule)
    {
        $this->formule = $formule;
    }

    public function generateFormule($atomes)
    {
        $str = '';
        $formule = [];
        foreach($atomes as $atome)
        {
            $formule[] = $atome->getSymbole();
        }

        foreach (array_count_values($formule) as $key => $value) {
            if($value == 1){
                $value = '';
            }
            $str .= $key . $value;
        }

        $this->setFormule($str);
    }

    /**
     * @return mixed
     */
    public function getAtomes()
    {
        return $this->atomes;
    }

    private function validateAtome(array $atomes){
        if($atomes instanceof Atome){
            return $this->atomes;
        }else{
            return false;
        }
    }

    /**
     * @param mixed $atomes
     */
    public function setAtomes(array $atomes)
    {
        if(count($atomes) >= 2)
        {
            $this->validateAtome($atomes);
            $this->atomes = $atomes;
        }else{
            trigger_error('Non 2 minimum !!!');
        }

    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCout()
    {
        return $this->cout;
    }

    /**
     * @param mixed $cout
     */
    public function setCout($cout)
    {
        $this->cout = $cout;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param mixed $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }


}
