<?php
namespace App;

class Type
{
    protected $type;

    public function __construct($type)
    {
        $this->setType($type);
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}