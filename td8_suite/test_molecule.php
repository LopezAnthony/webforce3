<?php

use App\Atome;
use App\Categorie;
use App\Experience;
use App\Fusion;
use App\Molecule;
use App\Type;

require "vendor/autoload.php";

//Instance Atome
$oxygene = new Atome('oxygene', 'O', 2);
$hydrogene = new Atome('hydrogene', 'H', 1);
$carbon = new Atome('carbone', 'C', 4);

//Instance Type
$type = new Type('Naturelle');

//Instance Categorie
$categorie = new Categorie('Glucide');

//Instance Molecule
$molecule = new Molecule('test', null,[$oxygene,$hydrogene, $hydrogene], $type, $categorie);
$molecule1 = new Molecule('test2', null, [$oxygene,$oxygene,$oxygene, $carbon], $type, $categorie);
var_dump($molecule);
var_dump($molecule1);
$date = new DateTime('NOW');

//Instance Fusion
$fusion = new Fusion();

//Instance Experience
$newMol = $fusion->getFusion($molecule, $molecule1);
$experience = New Experience(1, $date, 'Toto', $type, $newMol, 'moleculeFusionnée');
var_dump($experience);

?>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/alchemyjs/0.4.2/alchemy.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/alchemyjs/0.4.2/scripts/vendor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/alchemyjs/0.4.2/alchemy.js"></script>

    <style>
        .node text {
            display: block;
            text-shadow: 0 0 5px #000;
            font-size: 1.3em;
        }
    </style>
</head>
<body>
<div class="alchemy" id="alchemy"></div>

<?php
    $datasource = [];
    $atomesMolecule = $experience->getMolecule()->getAtomes();

    foreach($atomesMolecule as $key => $atome)
    {
        $tabNodes = [
            "id" => $key,
            "atom" => $atome->getSymbole(),
            "caption" => $atome->getSymbole()
        ];

        $tabEdges = [
          "source" =>3,
          "target" =>$key
        ];

        $datasource['nodes'][] = $tabNodes;
        $datasource['edges'][] = $tabEdges;

    }

    $json = json_encode($datasource);
?>
<script type="text/javascript">
    var json = JSON.parse('<?= $json ?>');
    alchemy.begin({
            graphHeight: function() {return 500;},
            graphWidth: function() {return 500;},
            forceLocked: false,
            nodeTypes: {"atom": ["C", "H", 'O']},
            nodeStyle: {
                "all": {
                    radius: 25
                },
                "C": {
                    color: "rgba(27, 158, 119, 1)",
                    borderColor: "rgba(27, 158, 119, .9)"
                },
                "H": {
                    color: "rgba(217, 95, 2, 1)",
                    borderColor: "rgba(217, 95, 2, .9)"
                },
                "O": {
                    color: "rgba(117, 112, 179, 1)",
                    borderColor: "rgba(117, 112, 179, .9)"
                }
            },
            edgeStyle: {
                "all": {
                    "width": 10
                }
            },
            dataSource: {  // accepte aussi en `string` un fichier `json` avec les mêmes données
                "nodes": json.nodes,
                "edges": json.edges
            }
        }
    );
</script>
